import base64
import paramiko

class SshCmd:
    def __init__(self, host):
        self.host = host
        key = paramiko.rsakey.RSAKey.from_private_key_file('/home/andrea/.ssh/id_rsa')
        self.client = paramiko.SSHClient()
        self.client.get_host_keys().add(host, 'ssh-rsa', key)
        self.client.load_system_host_keys()
    
    def connect(self):
        self.client.connect(self.host, port=2201, username='robot')

    def send_cmd(self, cmd):    
        stdin, stdout, stderr = self.client.exec_command(cmd)
        resp = ""
        for line in stdout:
            resp += line
        return resp

    def check_user(self, org_name):
        cmd_exists = "from django.contrib.auth.models import User; from labels.models import Company; dt = Company.objects.filter(name__contains='{}').count(); print(dt)".format(org_name)
        cmd_expires = "from django.contrib.auth.models import User; from labels.models import Company; dt = Company.objects.get(name__contains='{}').date_expired; print(dt)".format(org_name)
        cmd_perm_api = "from django.contrib.auth.models import User; from labels.models import Company; dt = Company.objects.get(name__contains='{}').role_set.first().profile.user.has_perm('labels.api_v1'); print(dt)".format(org_name)
        cmd_perm_ds_plus = "from django.contrib.auth.models import User; from labels.models import Company; dt = Company.objects.get(name__contains='{}').role_set.first().profile.user.has_perm('labels.view_datasheet_plus'); print(dt)".format(org_name)

        resp = self.send_cmd(
            """cd /home/robot/apps/mylabel;
            pipenv shell;
            $(pipenv --venv)/bin/python2.7 manage.py shell --command="{}";
            $(pipenv --venv)/bin/python2.7 manage.py shell --command="{}"
            $(pipenv --venv)/bin/python2.7 manage.py shell --command="{}"
            $(pipenv --venv)/bin/python2.7 manage.py shell --command="{}"
            """.format(cmd_exists, cmd_expires, cmd_perm_api, cmd_perm_ds_plus))
        
        user = {
            "exists": resp.split("\n")[0]
        }
        if user["exists"] != "0":
            user["expires"] = resp.split("\n")[1]
            user["perm_api"] = resp.split("\n")[2]
            user["perm_ds_plus"] = resp.split("\n")[3]
        return user

    def check_branch(self):
        resp = self.send_cmd(
            """cd /home/robot/apps/mylabel;
            git rev-parse --abbrev-ref HEAD;
            git log --format=%B -n 1
            """)
        print(resp)
        branch = {
            "name": resp.split("\n")[0],
            "commit": resp.split("\n")[1]
        }
        return branch

    def close(self):
        self.client.close()

if __name__ == '__main__':
    cmder = SshCmd("134.122.56.21")
    cmder.connect()
    branch = cmder.check_branch()
    print(branch)
    user = cmder.check_user("BASSINI")
    print(user)
    #cmder.close()