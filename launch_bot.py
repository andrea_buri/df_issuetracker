from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove, ChatAction)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                          ConversationHandler)

import logging
import os
from tracker import Tracker

TOKEN=os.environ["DF_ISSUETRACKER_TOKEN"]
CHAT_ID=int(os.environ["DF_ISSUETRACKER_CHATID"])
ASK_MACHINE, ASK_PROBLEM, ASK_ORGANIZATION, SUMMARY = range(4)

cnt = 0
def issue_cnt():
    global cnt
    cnt+=1
    return "{:03}".format(cnt)

def start(update, context):
    print(update.effective_chat.id)
    if update.effective_chat.id != CHAT_ID:
        return
    reply_keyboard = [['ciao']]

    update.message.reply_text(
        'Ciao, sono l\'issue tracker di Datafood.',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return ASK_MACHINE                   

def first(update, context):
    reply_keyboard = [['104.248.84.104', '134.122.56.21', 'beta.traccia.mylabel.it']]

    update.message.reply_text(
        'Aperto Issue #{}. Su quale server hai riscontrato il problema?'.format(issue_cnt()), 
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return ASK_PROBLEM

def machine(update, context):
    tracker.machine = update.message.text
    reply_keyboard = [['Server giù', 'Login'], ['Schede: esporta pdf', 'Traccia: import']]

    update.message.reply_text(
        'Quale problema hai riscontrato?', 
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return ASK_ORGANIZATION

def problem(update, context):
    tracker.problem = update.message.text
    reply_keyboard = [['BASSINI', 'soulk']]

    tracker.organization_name = update.message.text

    update.message.reply_text(
        'Quale organizzazione ha riscontrato il problema?', 
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return SUMMARY

def organization(update, context):
    tracker.organization = update.message.text
    context.bot.send_chat_action(chat_id=update.message.chat_id, action=ChatAction.TYPING)

    update.message.reply_text(
        tracker.report()
    )

    return ConversationHandler.END

def cancel(update, context):
    user = update.message.from_user
    #logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('La richiesta di issue tracking è stata annullata, ciao!')

    return ConversationHandler.END

def main():
    updater = Updater(token=TOKEN, use_context=True)
    dp = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('ciao', start)],

        states={
            ASK_MACHINE: [MessageHandler(Filters.text, first)],
            ASK_PROBLEM: [MessageHandler(Filters.text, machine)],
            ASK_ORGANIZATION: [MessageHandler(Filters.text, problem)],
            SUMMARY: [MessageHandler(Filters.text, organization)],
        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)

    # Start the Bot
    updater.start_polling()

    updater.idle()


if __name__ == '__main__':
    tracker = Tracker()
    main()