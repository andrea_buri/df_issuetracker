import requests

from ssh_cmder import SshCmd

class Tracker():
    def __init__(self, problem = None, organization = None, machine = None):
        self.problem = problem
        self.organization = organization
        self.machine = machine
        self._user_cache = None
        self._branch_cache = None

    def reachable_server(self):
        if not self.machine: 
            return "nessun server indicato"
        else:
            try:
                resp = requests.get(f"http://{self.machine}", timeout=5)
            except Exception as e:
                return "TIMEOUT"
            if resp.status_code >= 500:
                return "Errore server"
            elif resp.status_code >= 400:
                return "File not found"
            else:
                return f"{resp.status_code}, OK"

    def running_app(self):
        return "Check al momento assente"

    def env_check(self):
        return "Check al momento assente"

    def rephrase(self, word):
        if word == "1":
            return "SI"
        elif word == "0":
            return "NO"    
        elif word == "True":
            return "SI"
        elif word == "False":
            return "NO"
        return word

    def invalidate(self):
        self._user_cache = None
        self._branch_cache = None

    @property
    def branch(self):
        if self._branch_cache:
            return self._branch_cache
        if not self.machine:
            return {}
        if not getattr(self, "ssh_cmder", None):
            self.ssh_cmder = SshCmd(self.machine)            
            self.ssh_cmder.connect()        
        self._branch_cache = self.ssh_cmder.check_branch()
        return self._branch_cache

    @property
    def user(self):
        if self._user_cache:
            return self._user_cache
        if not self.machine:
            return {}
        if not getattr(self, "ssh_cmder", None):
            self.ssh_cmder = SshCmd(self.machine)            
            self.ssh_cmder.connect()        
        self._user_cache = self.ssh_cmder.check_user(self.organization)
        return self._user_cache

    def existing_organization(self):
        if not self.organization or not self.machine:
            return "Dato non ottenuto"

        if self.user.get('exists',None):
            return self.rephrase(self.user.get("exists"))
        return "NO"
            
    def active_organization(self):
        if not self.organization or not self.machine:
            return "Dato non ottenuto"

        if self.user.get('expires'):
            return self.user.get('expires')
        return "Dato non ottenuto"
        
    def has_ds_permission(self):
        if not self.organization or not self.machine:
            return "Dato non ottenuto"

        if self.user.get('perm_api'):
            return self.rephrase(self.user.get('perm_api'))
        return "Dato non ottenuto"

    def has_api_permission(self):
        if not self.organization or not self.machine:
            return "Dato non ottenuto"

        if self.user.get('perm_ds_plus'):
            return self.rephrase(self.user.get('perm_ds_plus'))
        return "Dato non ottenuto"

    def report(self):
        rep =  f"""Di seguito un resoconto del bot check per issue di tipo {self.problem}
Server {self.machine} raggiungible: {self.reachable_server()}
Ramo sviluppo: {self.branch['name']}
Ramo commit: {self.branch['commit']}
Check ambientale: {self.env_check()}
Utente {self.organization} esiste: {self.existing_organization()}
Utente {self.organization} scadenza: {self.active_organization()}
Utente {self.organization} ha permessi schede: {self.has_ds_permission()}
Utente {self.organization} ha permessi api: {self.has_api_permission()}
Basic check applicazione: OK
    """
        self.invalidate()
        return rep
        
if __name__ == "__main__":
    tracker = Tracker("login","BASSINI","134.122.56.21")
    print(tracker.report())